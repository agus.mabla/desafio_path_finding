<?php
if(count($_GET)>0){
	$inicio = json_decode(base64_decode($_GET["st"]));
	$fin = json_decode(base64_decode($_GET["fn"]));
	$obstaculos = json_decode(base64_decode($_GET["obst"]));
	$filas = $_GET["f"];
	$cols = $_GET["c"];
	$tablero = array();
	/*tablero[fila][columna]: [0]-> estado; que hay en la posision?  0 para nada, 1 para obstaculo, 2 para inicio, 3 para destino
							  [1] -> valor; costo minimo?
							  [2] -> fila valor; de que fila viene el costo min?
							  [3] -> col valor; de q col viene el cost min?
							  [4] -> flag para posterior dibujo. El camino elegido pasa x esa posicion?
	*/

	for($i = 0;$i<$filas;$i++){
		for($z = 0; $z<$cols;$z++){
			$tablero[$i][$z][0] = 0;
			$tablero[$i][$z][4] = 0;	 
		}
	}
	$tablero[$inicio[0]][$inicio[1]][0] = 2;
	$tablero[$inicio[0]][$inicio[1]][1] = 0;

	$tablero[$fin[0]][$fin[1]][0] = 3;

	foreach ($obstaculos as $obstaculo){
		$tablero[$obstaculo[0]][$obstaculo[1]][0] = 1;	
	}

	$cont_costo_max_act = 0;
	while(!isset($tablero[$fin[0]][$fin[1]][1])){
		//echo "a";
		
		for($f = 0; $f < $filas; $f++){
			//echo "b";
			for($c =0; $c < $cols; $c ++){
				//echo "c";
				if(isset($tablero[$f][$c][1])){
					//echo "d";
					if($tablero[$f][$c][1] === $cont_costo_max_act){
						//echo "e";
						for($i =0; $i<8; $i++){
							//echo "f";
							$pos_rel = posicion_relativa($f,$c,$i);
							if(!isset($tablero[$pos_rel[0]][$pos_rel[1]])){
								//echo "g";
								continue;
							} else if(isset($tablero[$pos_rel[0]][$pos_rel[1]][1])){
								//echo "h";
								continue;
							}else if($tablero[$pos_rel[0]][$pos_rel[1]][0] == 1){
								//echo "i";
								continue;
							}else{
								//echo "j";
								$tablero[$pos_rel[0]][$pos_rel[1]][1] = $cont_costo_max_act + 1;
								$tablero[$pos_rel[0]][$pos_rel[1]][2] = $f;
								$tablero[$pos_rel[0]][$pos_rel[1]][3] = $c;
							}
						}
					}
				}
			}
		}
		$cont_costo_max_act ++;
	}
	//X si te lo preguntabas, si, estaba bastante aburrido (?
	$pos_temp = array($fin[0],$fin[1]);
	$camino = "";
	while ($pos_temp[0] != $inicio[0] || $pos_temp[1] != $inicio[1]){
		$camino .= "[".$pos_temp[0].",".$pos_temp[1]."],";
		$fila_temp = $tablero[$pos_temp[0]][$pos_temp[1]][2];
		$col_temp =  $tablero[$pos_temp[0]][$pos_temp[1]][3];
		$tablero[$pos_temp[0]][$pos_temp[1]][4] = true;
		$pos_temp[0] = $fila_temp;
		$pos_temp[1] = $col_temp;
	}
	$camino .= "[".$inicio[0].",".$inicio[1]."]";

	echo "Costo = ".$tablero[$fin[0]][$fin[1]][1]."<br>";
	echo "Camino: ".$camino;
	echo dibujar($tablero,$filas,$cols);
	echo "<br><input type='button' value='Volver' id='volver'>";
	echo "
		<script src='js/jquery.js'></script>
		<script type='text/javascript'>
			$(document).ready(function(){
				$('#volver').click(function(){
					$(location).attr('href','front.html');
				});

			});
		</script>
		";
}
function dibujar($tab,$f,$c){
	$dibujo= "<table>";
	for($z =0; $z < $f;$z++){
		$dibujo .= "<tr>";
		for($w =0;$w<$c;$w++){
			switch($tab[$z][$w][0]){
				case 0:
					if(!$tab[$z][$w][4]){
						$dibujo .= "<td style='background-color:grey;'>&nbsp&nbsp&nbsp&nbsp&nbsp</td>";
					}else{
						$dibujo .= "<td style='background-color:red;'>&nbsp&nbsp&nbsp&nbsp&nbsp</td>";
					}
					break;
				case 1:
					$dibujo .= "<td style='background-color:black;'>&nbsp&nbsp&nbsp&nbsp&nbsp</td>";
					break;
				case 2:
					$dibujo .= "<td style='background-color:green;'>&nbsp&nbsp&nbsp&nbsp&nbsp</td>";
					break;
				case 3:
					$dibujo .= "<td style='background-color:blue;'>&nbsp&nbsp&nbsp&nbsp&nbsp</td>";
					break;
			}			
		}
		$dibujo .= "</tr>";
	}
	$dibujo .= "</table>";
	return $dibujo;
}
function posicion_relativa($f,$c,$numero){
	//$posicion;
	switch ($numero) {
		case 0:
			$posicion = array(($f-1),($c-1));
			break;
		case 1:
			$posicion = array(($f-1),($c));
			break;
		case 2:
			$posicion = array(($f-1),($c+1));
			break;
		case 3:
			$posicion = array(($f),($c-1));
			break;
		case 4:
			$posicion = array(($f),($c+1));
			break;
		case 5:
			$posicion = array(($f+1),($c-1));
			break;
		case 6:
			$posicion = array(($f+1),($c));
			break;
		case 7:
			$posicion = array(($f+1),($c+1));
			break;
	}
	return $posicion;
}

?>