<?php
$inicio = array(2,4);
$fin = array(1,7);
$filas = 8;
$cols = 8;
$tablero = array();
//tablero[fila][columna]: [0]-> estado; [1] -> valor; [2] -> fila valor; [3] -> col valor; [4] -> flag

for($i = 0;$i<$filas;$i++){
	for($z = 0; $z<$cols;$z++){
		$tablero[$i][$z][0] = 0;
		$tablero[$i][$z][1] = 0;
		$tablero[$i][$z][4] = 0;
		//echo $i.$z; 
	}
}
//echo !isset($tablero[-1]) ? "si" : "no";

for($i=0; $i<8; $i++){
	$posicion = posicion_relativa($inicio[0],$inicio[1],$i);
	if(isset($tablero[$posicion[0]][$posicion[1]])){
		if($tablero[$posicion[0]][$posicion[1]][0] ==0){
			$tablero[$posicion[0]][$posicion[1]][1] = 1;
			$tablero[$posicion[0]][$posicion[1]][2] = $inicio[0];
			$tablero[$posicion[0]][$posicion[1]][3] = $inicio[1];
		}
	}
	
}
//echo isset($tablero[5][3]);

echo valor($fin[0],$fin[1]);
if(valor($fin[0],$fin[1]) != -1){
	$camino="";
	$posicion_actual[0] = $fin[0];
	$posicion_actual[1] = $fin[1];
	while ($posicion_actual[0] != $inicio[0] || $posicion_actual[1] != $inicio[1]) {
		$camino .= "[".$posicion_actual[0].",".$posicion_actual[1]."],";
		$posicion_actual[0] = $tablero[$posicion_actual[0]][$posicion_actual[1]][2]; 
		$posicion_actual[1] = $tablero[$posicion_actual[0]][$posicion_actual[1]][3]; 
	}
	$camino .= "[".$inicio[0].",".$inicio[0]."]";
	echo $camino;
}

//echo json_encode($tablero);


function valor($f,$c){
	global $tablero;
	if(isset($tablero[$f][$c])  == 0){
		//echo "a".$f.$c;
		return (-1);
	}
	if($tablero[$f][$c][4]){
		//while ($tablero[$f][$c][1] == 0);
		return $tablero[$f][$c][1];
	}
	$tablero[$f][$c][4] = 1;
	if($tablero[$f][$c][0] != 0){
		$tablero[$f][$c][1] = -1;
		echo "b";
		return (-1);
	}
	/*
	if(isset($tablero[$f][$c][1])){
		return $tablero[$f][$c][1];
	}
	*/
	$costos = array();
	for($w= 0; $w < 8;$w++){
		$pos_temp = posicion_relativa($f,$c,$w);
		$costos[$w] = valor($pos_temp[0],$pos_temp[1]);
	}
	$pos_menor;
	for($z=0;$z<8;$z++){
		if($costos[$z] != -1){
			$pos_menor = $z;
			break;
		}
		if($z==7 && $costos[$z] == -1){
			$tablero[$f][$c][1] = -1;
			echo "c";
			return (-1);
		}
	}
	for($z=0;$z<8;$z++){
		if($costos[$z] < $costos[$pos_menor] && $costos[$z] != -1){
			$pos_menor = $z;
		}
	}
	$tablero[$f][$c][1] = $costos[$pos_menor] +1;
	$puntero = posicion_relativa($f,$c,$pos_menor);
	$tablero[$f][$c][2] = $puntero[0];
	$tablero[$f][$c][3] = $puntero[1];
	return $costos[$pos_menor] +1;
}

function posicion_relativa($f,$c,$numero){
	$posicion;
	switch ($numero) {
		case 0:
			$posicion = array(($f-1),($c-1));
			break;
		case 1:
			$posicion = array(($f-1),($c));
			break;
		case 2:
			$posicion = array(($f-1),($c+1));
			break;
		case 3:
			$posicion = array(($f),($c-1));
			break;
		case 4:
			$posicion = array(($f),($c+1));
			break;
		case 5:
			$posicion = array(($f+1),($c-1));
			break;
		case 6:
			$posicion = array(($f+1),($c));
			break;
		case 7:
			$posicion = array(($f+1),($c+1));
			break;
	}
	return $posicion;
}
?>